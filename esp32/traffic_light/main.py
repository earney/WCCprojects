import led
import time

_green=led.LED(5)
_yellow=led.LED(17)
_red=led.LED(19)

def update_state(green, yellow, red):
    if green:
       _green.on()
    else:
       _green.off()

    if yellow:
       _yellow.on()
    else:
       _yellow.off()

    if red:
       _red.on()
    else:
       _red.off()

_state='green'

while True:
	#print(_state)
	if _state == 'yellow':
           update_state(False, False, True)
           _state='red'
           time.sleep(2)
        elif _state == 'red':
           update_state(True, False, False)
           _state='green'
           time.sleep(2)
        elif _state == 'green':
           update_state(False, True, False)
           _state='yellow'
           time.sleep(1)
