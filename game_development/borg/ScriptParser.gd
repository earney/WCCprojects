extends Node

#signal next_step(value)

class code_block:
	var indent_level: String
	var lines: PoolStringArray
	var re_indent: RegEx
	
	func _init(_lines, _indent_level):
		self.indent_level = _indent_level
		self.lines=_lines
		self.re_indent = RegEx.new()
		self.re_indent.compile("^(?<indent>\\s*)\\S")
		
	func find_end_of_block_pos() -> int:
		for i in range(0, self.lines.size()):
			var result=self.re_indent.search(self.lines[i])
			if result:
				var _indent_level:String = result.get_string("indent")
				if _indent_level.length() <= self.indent_level.length():
					return i
			else:  #we have a problem.. it should always match
				print("we got a problem with finding the code block")
		return self.lines.size()

class If_statement:
	var remaining_lines
	var indent_level: String
	var condition_result: bool
	#var steps=[]
	var start_pos:int=0
	var end_pos:int=0
	var end_statement_pos:int=0
	
	func _init(lines, _indent_level, condition):
		self.remaining_lines=lines
		self.indent_level=_indent_level
		self.condition_result = self._evaluate_condition("bool(%s)" % condition)

	func parse():
		#print("remaining lines = ", self.remaining_lines)
		var block = code_block.new(self.remaining_lines, self.indent_level)
		var _end_pos:int=block.find_end_of_block_pos()
		var else_start=0
		var else_end=0
		
		var isa_else_block:bool = false
		
		if self.remaining_lines.size() > _end_pos:
			isa_else_block=self.remaining_lines[_end_pos] == self.indent_level+"else:"
		
		if isa_else_block:
			#we have an else block!
			else_start=_end_pos+2
			var _else_block = code_block.new(self.remaining_lines.slice(else_start, self.remaining_lines.size()), 
											 self.indent_level)
			else_end = else_start +  _else_block.find_end_of_block_pos()
			self.end_statement_pos = else_end + 1
		else:
			self.end_statement_pos = _end_pos + 1
		
		if self.condition_result:
			self.start_pos=1
			self.end_pos=_end_pos
		else: #condition is false
			#we need to see if we have an else at this indent level:
			if isa_else_block:
				#we have an else block!
				self.start_pos = else_start
				self.end_pos = else_end
			else:
				self.start_pos=end_statement_pos
				self.end_pos=end_statement_pos
			#if there is no else, our end_pos can be used
		print("pos=%d" % end_pos)
		print(self.condition_result, self.start_pos, self.end_pos, self.end_statement_pos)
		
	func get_end_position() -> int:
		return self.end_pos

	func get_start_position() -> int:
		return self.start_pos

	func _evaluate_condition(commands, variable_names = [], variable_values = []) -> bool:
		var expression = Expression.new()
		var error = expression.parse(commands, variable_names)
		if error != OK:
			#should put a popup here to output the error text
			push_error(expression.get_error_text())
			return false

		var result = expression.execute(variable_values, self)

		if expression.has_execute_failed():
			#should put a popup here for the error.
			print("execute failed: ", str(result))
			return false

		print("execute successful: ", str(result))
		return result

class repeat_statement:
	var remaining_lines
	var indent_level: String
	var condition_result: bool
	#var steps=[]
	var start_pos:int=0
	var end_pos:int=0
	var end_statement_pos:int=0
	var num_repeats=0
	var iteration=0
	
	func _init(lines, _indent_level, _num_repeats):
		self.remaining_lines=lines
		self.indent_level=_indent_level
		self.num_repeats=_num_repeats
		
	func parse():
		print("remaining lines = ", self.remaining_lines)
		var block = code_block.new(self.remaining_lines, self.indent_level)
		var _end_pos:int=block.find_end_of_block_pos()
		
		self.end_statement_pos = _end_pos + 1
		
		self.start_pos=1
		self.end_pos=_end_pos
		
	func evaluate() -> bool:
		self.iteration+=1
		return self.iteration <= self.num_repeats
		
	func get_end_position() -> int:
		return self.end_pos

	func get_start_position() -> int:
		return self.start_pos



class_name ScriptParser
var lines=[]

var re: RegularExpression
var pos: int = 0
var player_node
var script_node
var step_node

func _init(text, _player_node, _script_node, _step_node):
	self.player_node=_player_node
	self.script_node=_script_node
	self.step_node=_step_node
	self.lines = text.split("\n")	
	self.re = RegularExpression.new()
	self.pos=0
	
func _parse_line(line_number):
	var _next_pos=line_number+1
	print("_parse_line ", line_number)
	#var _steps=[]
	var _line=self.lines[line_number]
	var _result=self.re.isa_if_statement(_line)
	if _result:
		var _indent:String=_result.get_string("indent")
		var _condition=_result.get_string("condition")
		var _if
		if line_number + 1 == self.lines.size() -1:
			var _array:Array=Array(self.lines).slice(self.lines.size() - 1, self.lines.size())
			_if = If_statement.new(_array, _indent, _condition)
		else:	
			_if = If_statement.new(Array(self.lines).slice(line_number+1, self.lines.size()), _indent, _condition)
		
		_if.parse()
		self.player_node.call_deferred("pass", line_number)
		self.step_node.step_submitted=true
		yield(player_node, "got_pass")
		yield(self.player_node, "waiting_for_step")
	
		var start_pos = _if.get_start_position()
		var end_pos=_if.get_end_position()
		print(start_pos, " ", end_pos)
		if start_pos < end_pos:  #if start_pos < end_pos our if statement is true
			_next_pos=pos+end_pos+1
			for i in range(pos+start_pos, pos+end_pos+1):
				self._parse_line(i)
				yield(self.player_node, "waiting_for_step")
		self.pos=_next_pos
		return 0
	
	
	_result=self.re.isa_move_statement(_line)
	if _result:
		var _indent=_result.get_string("indent")
		self.pos+=1
		#queue_free()
		#self.script_node.emit_signal("next_step", Step.new(line_number, "move()"))
		
		self.player_node.call_deferred("move", line_number)
		#print("yielding")
		self.step_node.step_submitted=true
		yield(self.player_node, "got_move")
		return 0
		
	_result=self.re.isa_turn_right_statement(_line)
	if _result:
		var _indent=_result.get_string("indent")
		self.pos+=1
		#queue_free()
		self.player_node.call_deferred("turn_right", line_number)
		#self.script_node.emit_signal("next_step", Step.new(line_number, "turn_right()"))
		#print("yielding")
		self.step_node.step_submitted=true
		yield(self.player_node, "got_turn_right")
		return 0
	
	_result=self.re.isa_empty_statement(_line)
	if _result:
		self.pos+=1
		return 0

	#check to see if this is a user defined function
	
	print("invalid command:'%s'" % _line)
	return 0


func parse_script():
	if self.pos < self.lines.size():
		return self._parse_line(self.pos)
	
	self.step_node.script_finished=true
