extends Area2D

onready var tween = $Tween
onready var ray = $RayCast2D
onready	var script_node = get_node("/root/Main/MarginContainer/Panel/HSplitContainer/VBoxContainer2/TabContainer/UserScript")

onready var step_node = get_node("/root/Main/MarginContainer/Panel/HSplitContainer/VBoxContainer/HBoxContainer/StepButton")

export var speed = 3

var action:String
var isa_pass=false
signal waiting_for_step(value)
signal got_move
signal got_turn_right
signal got_pass

var direction = "south"
var direction_map = {"west" : "left",
					 "east": "right",
					 "north" : "up",
					 "south": "down"}


var tile_size = 32
var inputs = {"right": Vector2.RIGHT,
			"left": Vector2.LEFT,
			"up": Vector2.UP,
			"down": Vector2.DOWN}
	
func _ready():
	#action=""
	#script_node.connect("next_step", self, "_next_step")
	#print("waiting_for_step")
	#emit_signal("waiting_for_step", true)
	#position = Vector2.ONE * tile_size * 1.5
	_init()
	$AnimationPlayer.playback_speed = speed
	
	
func _myinit():
	action=""
	position = Vector2(55, 75)
	# Adjust animation speed to match movement speed
	_turn("down")
	direction="south"

#func _next_step(_step):
#	#print("player:next_action -> ", _step.action)
#	#script_node.cursor_set_line(_step.line_number)
#	self.action = _step.action
	

func _process(_delta):
#process(delta):
# use this if you want to only move on keypress
# func _unhandled_input(event):
	if self.isa_pass:
		return
		
	if tween.is_active():
		return

func _turn(dir):
	ray.cast_to = inputs[dir] * tile_size
	ray.force_raycast_update()
	$AnimationPlayer.play(dir)
	turn_tween(inputs[dir])

func _move(dir):
	ray.cast_to = inputs[dir] * tile_size
	ray.force_raycast_update()
	if !ray.is_colliding():
		$AnimationPlayer.play(dir)
		move_tween(inputs[dir])

func turn_tween(dir):
	tween.interpolate_property(self, "position",
		position, position + dir * 1,
		1.0/speed, Tween.TRANS_SINE, Tween.EASE_IN_OUT)	
	tween.start()
		
func move_tween(dir):
	tween.interpolate_property(self, "position",
		position, position + dir * tile_size,
		1.0/speed, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.start()

func pass(line_number):
	script_node.cursor_set_line(line_number)
	yield(get_tree().create_timer(0.3), "timeout")
	emit_signal("got_pass", true)
	

func turn_right(line_number):
	script_node.cursor_set_line(line_number)
	print("turn_right", direction)
	if direction == "south":
		direction = "west"
	elif direction == "west":
		direction = "north"
	elif direction == "north":
		direction = "east"
	elif direction == "east":
		direction = "south"
	
	#print(direction)
	_turn(direction_map[direction])
	yield(get_tree().create_timer(1.0), "timeout")
	emit_signal("got_turn_right")
	#
func move(line_number):
	script_node.cursor_set_line(line_number)	
	print("call move")
	_move(direction_map[direction])
	yield(get_tree().create_timer(1.0), "timeout")
	emit_signal("got_move")
	#
