extends Button

onready var run_node = get_node("../RunButton")
onready var step_node = get_node("../StepButton")
onready var player_node = get_node("/root/Main/MarginContainer/Panel/HSplitContainer/VBoxContainer/ViewportContainer/Viewport/Player")


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _pressed():
	#resets the whole environment
	step_node._step_init()
	player_node._myinit()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
