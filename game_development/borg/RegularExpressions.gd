extends Node

class_name RegularExpression
var re_if=RegEx.new()
#var re_else=RegEx.new()   #if_else..
var re_move=RegEx.new()
var re_turn_right=RegEx.new()
var re_empty_line=RegEx.new()
var re_repeat=RegEx.new()

func _init():
	self.re_if.compile("^(?<indent>\\s*)if (?<condition>[^:]+):")
	#self.re_else.compile("^(?<indent>\\s*)else:")
	self.re_move.compile("^(?<indent>\\s*)move\\(\\)")
	self.re_turn_right.compile("^(?<indent>\\s*)turn_right\\(\\)")
	self.re_empty_line.compile("^\\s*$")
	self.re_repeat.compile("^(?<indent>\\s*)repeat (?<number>\\d+):")

func isa_move_statement(line) -> bool:
	return self.re_move.search(line)
	
func isa_turn_right_statement(line) -> bool:
	return self.re_turn_right.search(line)
	
func isa_if_statement(line) -> bool:
	return self.re_if.search(line)

func isa_repeat_statement(line) -> bool:
	return self.re_repeat.search(line)
#func isa_else_statement(line) -> bool:
#	return self.re_else.search(line)

func isa_empty_statement(line) -> bool:
	return self.re_empty_line.search(line)
