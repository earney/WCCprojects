extends Button

onready var run_node = get_node("../RunButton")
onready var step_node = get_node("../StepButton")
onready var player_node = get_node("/root/Main/MarginContainer/Panel/HSplitContainer/VBoxContainer/ViewportContainer/Viewport/Player")


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _pressed():
	print("stop button pressed")
	if run_node.executing:
		run_node.executing=false
		player_node.disconnect("waiting_for_step", step_node, "_step")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
