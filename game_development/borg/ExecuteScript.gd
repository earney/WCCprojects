extends Button

onready var step_node = get_node("../StepButton")
onready var player_node = get_node("/root/Main/MarginContainer/Panel/HSplitContainer/VBoxContainer/ViewportContainer/Viewport/Player")

var executing=false

# Called when the node enters the scene tree for the first time.
func _ready():
	#player_node.connect("waiting_signal", self, "_step")
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _pressed():
	executing=true
	step_node._step_init()
	player_node.connect("waiting_for_step", step_node, "_step")
