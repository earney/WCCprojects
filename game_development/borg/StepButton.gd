extends Button

onready var player_node = get_node("/root/Main/MarginContainer/Panel/HSplitContainer/VBoxContainer/ViewportContainer/Viewport/Player")
onready	var script_node = get_node("/root/Main/MarginContainer/Panel/HSplitContainer/VBoxContainer2/TabContainer/UserScript")
onready var step_node = get_node("/root/Main/MarginContainer/Panel/HSplitContainer/VBoxContainer/HBoxContainer/StepButton")
	
var step_pos=0
var steps=[]
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var parser: ScriptParser

var _old_text:String=""

var yield_result

var step_submitted:bool = false
var script_finished:bool = false

#var running:bool = true
# Called when the node enters the scene tree for the first time.
func _ready():
	self._step_init()
	
func script_finished() -> bool:
	return self.parser.pos >= self.parser.lines.size()
	
func _pressed():
	if disabled:
		return
		
	step_submitted=false
	self.disabled=true
	#self._step_init()\
	if not self.script_finished():
		print("waiting_for_step")
		self.player_node.emit_signal("waiting_for_step", true)
	
	if _old_text != script_node.text:
		print("old text != text")
		_old_text=script_node.text
		self._step_init()
	
		self.yield_result = self.parser.parse_script()

	#self.script_finished=self.parser.pos == self.parser.lines.size()
	var i=0
	while not self.step_submitted and not self.script_finished():
		i+=1
		if i>1000:
			print("i > 1000")
			self.step_submitted=true
			self.disabled=false
			return
		while not self.yield_result is int:
			if self.step_submitted:
				self.disabled=false
				return
			if self.yield_result is GDScriptFunctionState and self.yield_result.is_valid():
				self.yield_result=self.yield_result.resume()
			else:
				self.yield_result=self.parser.parse_script()
			
		#self.script_finished=self.parser.pos == self.parser.lines.size()
	
	if self.script_finished():
		#display a window that says execution finished!
		var _win=AcceptDialog.new()
		print("display finished window")
		_win.set_text("Execution Finished")
		_win.connect('modal_closed', _win, 'queue_free')
		self.add_child(_win)
		_win.popup_centered()
		
	self.disabled=false
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _step_init():
	script_node.highlight_current_line=true
	script_node.cursor_set_line(0)
	self.parser = ScriptParser.new(script_node.text, player_node, script_node, step_node)
	self.step_submitted=true
	self.disabled=false
	self.yield_result=0
	
func __step(player_waiting):
	#print("executing step", value)
	
	if self.yield_result is GDScriptFunctionState and self.yield_result.is_valid():
		self.yield_result=self.yield_result.resume()
	return
